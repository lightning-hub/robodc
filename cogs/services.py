from datetime import datetime
from typing import NamedTuple, Optional
import logging

import asyncio
import aiohttp
from discord.ext import commands, tasks

from .utils.formats import format_dt

log: logging.Logger = logging.getLogger(__name__)
SERVICES_CHANNEL = 893508863712690207


class Status(NamedTuple):
    status: int
    reason: Optional[str]


class Services(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Ping task
        self.ping_task.start()
        self.websites = {"https://udb-api.lightsage.dev/": False,
                         "https://paste.lightsage.dev/": False}

        # Integrity check
        self.udb_cache_integrity.start()
        self._last_integrity = None
        self._notified = False

    def cog_unload(self) -> None:
        self.udb_cache_integrity.cancel()
        self.ping_task.cancel()

    async def request(self, site: str) -> Status:
        try:
            async with self.bot.session.get(site, timeout=aiohttp.ClientTimeout(total=60)) as resp:
                return Status(resp.status, resp.reason)
        except asyncio.TimeoutError:
            return Status(404, "Timed out")
        except Exception as e:
            log.error(e)

    @tasks.loop(seconds=60.0)
    async def ping_task(self) -> None:
        for key, value in self.websites.items():
            e = await self.request(key)
            if e.status != 200:
                # We only need one warning message
                if value:
                    return

                channel = self.bot.get_channel(SERVICES_CHANNEL)
                await channel.send(f"\N{WARNING SIGN} Got `{e.status}` with reason `{e.reason}` while trying to ping "
                                   f"`{key}`.")
                self.websites[key] = True
            else:
                # we recovered
                if value:
                    self.websites[key] = False
                    channel = self.bot.get_channel(SERVICES_CHANNEL)
                    await channel.send(f"\N{PARTY POPPER} `{key}` has recovered!")

    @ping_task.before_loop
    async def before_ping_task(self):
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=30)
    async def udb_cache_integrity(self):
        async with self.bot.session.get("https://udb-api.lightsage.dev/stats") as resp:
            if resp.status != 200:
                return
            data = await resp.json()
        
        if self._last_integrity is None:
            self._last_integrity = datetime.fromisoformat(data['last_update'])
            return

        if datetime.fromisoformat(data['last_update']) != self._last_integrity:
            self._notified = False
            return

        if datetime.fromisoformat(data['last_update']) == self._last_integrity:
            if self._notified is True:
                return
            channel = self.bot.get_channel(SERVICES_CHANNEL)
            await channel.send("\N{POLICE CARS REVOLVING LIGHT} UDB-API has not been refreshed since "
                               f"{format_dt(datetime.fromisoformat(data['last_update']))}")
            self._notified = True

    @udb_cache_integrity.before_loop
    async def before_loop(self):
        await self.bot.wait_until_ready()


async def setup(bot) -> None:
    await bot.add_cog(Services(bot))
