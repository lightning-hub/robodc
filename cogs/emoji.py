from __future__ import annotations
from typing_extensions import Annotated

from discord.ext import commands, tasks
from discord import app_commands
from .utils import checks, config

from collections import Counter, defaultdict
from typing import TYPE_CHECKING, Optional

import discord
import asyncio
import asyncpg
import datetime
import logging
import yarl
import re
import io

if TYPE_CHECKING:
    from bot import RoboDanny
    from .utils.context import GuildContext, Context

log = logging.getLogger(__name__)

BLOB_GUILD_ID = 272885620769161216
ROO_EMOJI_IDS = [604331487583535124, 604446987844190228, 606517600167526498, 610921560068456448]
ROO_EMOJI_IGNORED_CHANNEL_IDS = [604331488049365042, 604447679635783690, 606517971430539276, 610921684727365652,  # emoji-list channels
                                 626400077291716611,  # default furret channel
                                 ]
EMOJI_REGEX = re.compile(r'<a?:.+?:([0-9]{15,21})>')
EMOJI_NAME_REGEX = re.compile(r'^[0-9a-zA-Z\_]{2,32}$')


class BlobEmoji(commands.Converter):
    async def convert(self, ctx: GuildContext, argument: str) -> discord.Emoji:
        guild = ctx.bot.get_guild(BLOB_GUILD_ID)
        assert guild is not None

        emojis = {e.id: e for e in guild.emojis}

        m = EMOJI_REGEX.match(argument)
        if m is not None:
            emoji = emojis.get(int(m.group(1)))
        elif argument.isdigit():
            emoji = emojis.get(int(argument))
        else:
            emoji = discord.utils.find(lambda e: e.name == argument, emojis.values())

        if emoji is None:
            raise commands.BadArgument('Not a valid blob emoji.')
        return emoji


def partial_emoji(argument: str, *, regex=EMOJI_REGEX) -> int:
    if argument.isdigit():
        # assume it's an emoji ID
        return int(argument)

    m = regex.match(argument)
    if m is None:
        raise commands.BadArgument("That's not a custom emoji...")
    return int(m.group(1))


def emoji_name(argument: str, *, regex=EMOJI_NAME_REGEX) -> str:
    m = regex.match(argument)
    if m is None:
        raise commands.BadArgument('Invalid emoji name.')
    return argument


class EmojiURL:
    def __init__(self, *, animated: bool, url: str):
        self.url: str = url
        self.animated: bool = animated

    @classmethod
    async def convert(cls, ctx: GuildContext, argument: str) -> EmojiURL:
        try:
            partial = await commands.PartialEmojiConverter().convert(ctx, argument)
        except commands.BadArgument:
            try:
                url = yarl.URL(argument)
                if url.scheme not in ('http', 'https'):
                    raise RuntimeError
                path = url.path.lower()
                if not path.endswith(('.png', '.jpeg', '.jpg', '.gif')):
                    raise RuntimeError
                return cls(animated=url.path.endswith('.gif'), url=argument)
            except Exception:
                raise commands.BadArgument('Not a valid or supported emoji URL.') from None
        else:
            return cls(animated=partial.animated, url=str(partial.url))


def usage_per_day(dt: datetime.datetime, usages: int) -> float:
    tracking_started = datetime.datetime(2017, 3, 31, tzinfo=datetime.timezone.utc)
    now = discord.utils.utcnow()
    if dt < tracking_started:
        base = tracking_started
    else:
        base = dt

    days = (now - base).total_seconds() / 86400  # 86400 seconds in a day
    if int(days) == 0:
        return usages
    return usages / days


class Emoji(commands.Cog):
    """Custom emoji tracking"""

    def __init__(self, bot: RoboDanny):
        self.bot: RoboDanny = bot
        self.emoji_streak = config.Config("emoji_streak.json")
        self.bot.loop.create_task(self.check_messages_since_downtime())
        self._batch_of_data: defaultdict[int, Counter[int]] = defaultdict(Counter)
        self._batch_lock = asyncio.Lock(loop=bot.loop)
        self.bulk_insert.add_exception_type(asyncpg.PostgresConnectionError)
        self.bulk_insert.start()

    @property
    def display_emoji(self) -> discord.PartialEmoji:
        return discord.PartialEmoji(name='\N{LOWER LEFT PAINTBRUSH}\ufe0f')

    def cog_unload(self):
        self.bulk_insert.stop()

    async def cog_command_error(self, ctx: Context, error: commands.CommandError):
        if isinstance(error, commands.BadArgument):
            await ctx.send(str(error))

    async def check_messages_since_downtime(self):
        x = self.emoji_streak.all()
        for channel_id in x:
            try:
                channel = self.bot.get_channel(int(channel_id)) or (await self.bot.fetch_channel(int(channel_id)))
            except Exception:
                continue

            message_id = x[channel_id]["last_message_id_tracked"]
            async for message in channel.history(after=discord.Object(id=message_id)):
                await self.on_message(message)

    @tasks.loop(seconds=60.0)
    async def bulk_insert(self):
        query = """INSERT INTO emoji_stats (guild_id, emoji_id, total)
                   SELECT x.guild, x.emoji, x.added
                   FROM jsonb_to_recordset($1::jsonb) AS x(guild BIGINT, emoji BIGINT, added INT)
                   ON CONFLICT (guild_id, emoji_id) DO UPDATE
                   SET total = emoji_stats.total + excluded.total;
                """

        async with self._batch_lock:
            transformed = [
                {'guild': guild_id, 'emoji': emoji_id, 'added': count}
                for guild_id, data in self._batch_of_data.items()
                for emoji_id, count in data.items()
            ]
            self._batch_of_data.clear()
            await self.bot.pool.execute(query, transformed)

    def find_all_emoji(self, message, *, regex=EMOJI_REGEX):
        return regex.findall(message.content)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.guild is None:
            return

        if message.guild.id not in ROO_EMOJI_IDS or message.channel.id in ROO_EMOJI_IGNORED_CHANNEL_IDS:
            return

        if message.author.bot:
            return

        if message.is_system():  # Ignore system messages like <x> joined.
            return

        streak = self.emoji_streak.get(message.channel.id, {'last_message_id_tracked': None})
        emojis = [int(e) for e in self.find_all_emoji(message)]

        streak['last_message_id_tracked'] = message.id
        if "emoji" in streak:
            if streak['emoji']['id'] in emojis:
                streak['emoji']['counter'] += 1
                await self.emoji_streak.put(message.channel.id, streak)
            else:
                emoji = discord.utils.get(message.guild.emojis, id=streak['emoji']['id'])
                await message.channel.send(f"Wow what a loser, {message.author.mention} ruined the streak!\n"
                                           f"Streak for {str(emoji)} lasted for {streak['emoji']['counter']} messages.")
                streak.pop("emoji")
                await self.emoji_streak.put(message.channel.id, streak)
        elif len(emojis) == 0:
            # This is probably a normal message, but we need to track message ids
            await self.emoji_streak.put(message.channel.id, streak)
            return
        else:
            streak.update({"emoji": {"id": emojis[0], "counter": 1}})
            await self.emoji_streak.put(message.channel.id, streak)
            emoji = discord.utils.get(message.guild.emojis, id=emojis[0])
            await message.channel.send(f"Starting new streak for {str(emoji)}")

    @commands.Cog.listener()
    async def on_guild_emojis_update(
        self, guild: discord.Guild, before: tuple[discord.Emoji, ...], after: tuple[discord.Emoji, ...]
    ):
        # we only care when an emoji is added
        lookup = {e.id for e in before}
        added = [e for e in after if e.id not in lookup and len(e.roles) == 0]
        if len(added) == 0:
            return

        log.info('Server %s has added %s emojis.', guild, len(added))
        if guild.id != BLOB_GUILD_ID:
            return  # not the guild we care about

        # this is the backup channel
        channel: Optional[discord.TextChannel] = self.bot.get_channel(305841865293430795)  # type: ignore
        if channel is None:
            return

        for emoji in added:
            async with self.bot.session.get(emoji.url) as resp:
                if resp.status != 200:
                    continue

                data = io.BytesIO(await resp.read())
                await channel.send(emoji.name, file=discord.File(data, f'{emoji.name}.png'))
                await asyncio.sleep(1)

    async def get_stats_for(self, ctx: GuildContext, emoji: discord.Emoji):
        e = discord.Embed(colour=0xf1c40f, title='Statistics')

        query = """SELECT COALESCE(SUM(total), 0) AS "Count"
                   FROM emoji_stats
                   WHERE emoji_id=$1
                   GROUP BY emoji_id;
                """

        usage_row: tuple[int] = await ctx.db.fetchrow(query, emoji.id)  # type: ignore
        usage = usage_row[0]

        e.add_field(name='Emoji', value=emoji)
        e.add_field(name='Usage', value=f'{usage}, {usage_per_day(emoji.created_at, usage):.2f}/day')
        await ctx.send(embed=e)

    @commands.command(aliases=['roopost'], hidden=True)
    @checks.is_in_guilds(*ROO_EMOJI_IDS)
    @checks.is_admin()
    async def roosort(self, ctx):
        """Sorts roos"""
        emojis = sorted([e.name for e in ctx.guild.emojis if len(e.roles) == 0])
        paginator = commands.Paginator(suffix='', prefix='')
        for emoji in emojis:
            paginator.add_line(f'{emoji}: `{emoji}`')

        for page in paginator.pages:
            await ctx.send(page)

    @commands.hybrid_group(name='emoji')
    @commands.guild_only()
    @app_commands.guild_only()
    async def _emoji(self, ctx: GuildContext):
        """Emoji management commands."""
        await ctx.send_help(ctx.command)

    @_emoji.command(name='create')
    @checks.has_guild_permissions(manage_emojis=True)
    @commands.guild_only()
    @app_commands.describe(
        name='The emoji name',
        file='The image file to use for uploading',
        emoji='The emoji or its URL to use for uploading',
    )
    async def _emoji_create(
        self,
        ctx: GuildContext,
        name: Annotated[str, emoji_name],
        file: Optional[discord.Attachment],
        *,
        emoji: Optional[str],
    ):
        """Create an emoji for the server under the given name.

        You must have Manage Emoji permission to use this.
        The bot must have this permission too.
        """
        if not ctx.me.guild_permissions.manage_emojis:
            return await ctx.send('Bot does not have permission to add emoji.')

        reason = f'Action done by {ctx.author} (ID: {ctx.author.id})'

        if file is None and emoji is None:
            return await ctx.send('Missing emoji file or url to upload with')

        if file is not None and emoji is not None:
            return await ctx.send('Cannot mix both file and url arguments, choose only')

        is_animated = False
        request_url = ''
        if emoji is not None:
            upgraded = await EmojiURL.convert(ctx, emoji)
            is_animated = upgraded.animated
            request_url = upgraded.url
        elif file is not None:
            if not file.filename.endswith(('.png', '.jpg', '.jpeg', '.gif')):
                return await ctx.send('Unsupported file type given, expected png, jpg, or gif')

            is_animated = file.filename.endswith('.gif')
            request_url = file.url

        emoji_count = sum(e.animated == is_animated for e in ctx.guild.emojis)
        if emoji_count >= ctx.guild.emoji_limit:
            return await ctx.send('There are no more emoji slots in this server.')

        async with self.bot.session.get(request_url) as resp:
            if resp.status >= 400:
                return await ctx.send('Could not fetch the image.')
            if int(resp.headers['Content-Length']) >= (256 * 1024):
                return await ctx.send('Image is too big.')

            data = await resp.read()
            coro = ctx.guild.create_custom_emoji(name=name, image=data, reason=reason)
            async with ctx.typing():
                try:
                    created = await asyncio.wait_for(coro, timeout=10.0)
                except asyncio.TimeoutError:
                    return await ctx.send('Sorry, the bot is rate limited or it took too long.')
                except discord.HTTPException as e:
                    return await ctx.send(f'Failed to create emoji somehow: {e}')
                else:
                    return await ctx.send(f'Created {created}')

    def emoji_fmt(self, emoji_id: int, count: int, total: int) -> str:
        emoji = self.bot.get_emoji(emoji_id)
        if emoji is None:
            name = f'[\N{WHITE QUESTION MARK ORNAMENT}](https://cdn.discordapp.com/emojis/{emoji_id}.png)'
            emoji = discord.Object(id=emoji_id)
        else:
            name = str(emoji)

        per_day = usage_per_day(emoji.created_at, count)
        p = count / total
        return f'{name}: {count} uses ({p:.1%}), {per_day:.1f} uses/day.'

    async def get_guild_stats(self, ctx: GuildContext) -> None:
        e = discord.Embed(title='Emoji Leaderboard', colour=discord.Colour.blurple())

        query = """SELECT
                       COALESCE(SUM(total), 0) AS "Count",
                       COUNT(*) AS "Emoji"
                   FROM emoji_stats
                   WHERE guild_id=$1
                   GROUP BY guild_id;
                """
        record = await ctx.db.fetchrow(query, ctx.guild.id)
        if record is None:
            await ctx.send('This server has no emoji stats...')
            return

        total = record['Count']
        emoji_used = record['Emoji']

        assert ctx.me.joined_at is not None
        per_day = usage_per_day(ctx.me.joined_at, total)
        e.set_footer(text=f'{total} uses over {emoji_used} emoji for {per_day:.2f} uses per day.')

        query = """SELECT emoji_id, total
                   FROM emoji_stats
                   WHERE guild_id=$1
                   ORDER BY total DESC
                   LIMIT 10;
                """

        top = await ctx.db.fetch(query, ctx.guild.id)

        e.description = '\n'.join(f'{i}. {self.emoji_fmt(emoji, count, total)}' for i, (emoji, count) in enumerate(top, 1))
        await ctx.send(embed=e)

    async def get_emoji_stats(self, ctx: GuildContext, emoji_id: int) -> None:
        e = discord.Embed(title='Emoji Stats')
        cdn = f'https://cdn.discordapp.com/emojis/{emoji_id}.png'

        # first verify it's a real ID
        async with ctx.session.get(cdn) as resp:
            if resp.status == 404:
                e.description = "This isn't a valid emoji."
                e.set_thumbnail(url='https://this.is-serious.business/09e106.jpg')
                await ctx.send(embed=e)
                return

        e.set_thumbnail(url=cdn)

        # valid emoji ID so let's use it
        query = """SELECT guild_id, SUM(total) AS "Count"
                   FROM emoji_stats
                   WHERE emoji_id=$1
                   GROUP BY guild_id;
                """

        records = await ctx.db.fetch(query, emoji_id)
        transformed = {k: v for k, v in records}
        total = sum(transformed.values())

        dt = discord.utils.snowflake_time(emoji_id)

        # get the stats for this guild in particular
        try:
            count = transformed[ctx.guild.id]
            per_day = usage_per_day(dt, count)
            value = f'{count} uses ({count / total:.2%} of global uses), {per_day:.2f} uses/day'
        except KeyError:
            value = 'Not used here.'

        e.add_field(name='Server Stats', value=value, inline=False)

        # global stats
        per_day = usage_per_day(dt, total)
        value = f'{total} uses, {per_day:.2f} uses/day'
        e.add_field(name='Global Stats', value=value, inline=False)
        e.set_footer(text='These statistics are for servers I am in')
        await ctx.send(embed=e)

    @_emoji.group(name='stats', fallback='show')
    @commands.guild_only()
    @app_commands.describe(emoji='The emoji to show stats for. If not given then it shows server stats')
    async def emojistats(self, ctx: GuildContext, *, emoji: Annotated[Optional[int], partial_emoji] = None):
        """Shows you statistics about the emoji usage in this server.

        If no emoji is given, then it gives you the top 10 emoji used.
        """

        if emoji is None:
            await self.get_guild_stats(ctx)
        else:
            await self.get_emoji_stats(ctx, emoji)

    @emojistats.command(name='server', aliases=['guild'])
    @commands.guild_only()
    async def emojistats_guild(self, ctx: GuildContext):
        """Shows you statistics about the local server emojis in this server."""
        emoji_ids = [e.id for e in ctx.guild.emojis]

        if not emoji_ids:
            await ctx.send('This guild has no custom emoji.')

        query = """SELECT emoji_id, total
                   FROM emoji_stats
                   WHERE guild_id=$1 AND emoji_id = ANY($2::bigint[])
                   ORDER BY total DESC
                """

        e = discord.Embed(title='Emoji Leaderboard', colour=discord.Colour.blurple())
        records = await ctx.db.fetch(query, ctx.guild.id, emoji_ids)

        total = sum(a for _, a in records)
        emoji_used = len(records)

        assert ctx.me.joined_at is not None
        per_day = usage_per_day(ctx.me.joined_at, total)
        e.set_footer(text=f'{total} uses over {emoji_used} emoji for {per_day:.2f} uses per day.')
        top = records[:10]
        value = '\n'.join(self.emoji_fmt(emoji, count, total) for (emoji, count) in top)
        e.add_field(name=f'Top {len(top)}', value=value or 'Nothing...')

        record_count = len(records)
        if record_count > 10:
            bottom = records[-10:] if record_count >= 20 else records[-record_count + 10 :]
            value = '\n'.join(self.emoji_fmt(emoji, count, total) for (emoji, count) in bottom)
            e.add_field(name=f'Bottom {len(bottom)}', value=value)

        await ctx.send(embed=e)

    @commands.command(aliases=['listrooemojis'])
    @commands.has_permissions(manage_emojis=True)
    async def refreshemojilist(self, ctx: commands.Context) -> None:
        """Refreshes the emoji list channel.

        This purges 25 messages from the emoji-list channel and sends a new list of emojis
        """
        if ctx.guild.id not in ROO_EMOJI_IDS:
            return

        if len(ctx.guild.emojis) == 0:
            await ctx.send("This server has no emotes!")
            return

        channel = discord.utils.get(ctx.guild.channels, name="emoji-list")
        if not channel:
            return

        # We shouldn't have more than 25 messages in the emoji-list channel
        try:
            await channel.purge(limit=25)
        except Exception as e:
            # Notify that we tried
            await ctx.send(f"Somehow failed to purge messages: `{e}`")

        paginator = commands.Paginator(suffix='', prefix='')
        for emoji in ctx.guild.emojis:
            paginator.add_line(f'{emoji} -- `{emoji}`')

        for page in paginator.pages:
            await channel.send(page)

    @commands.Cog.listener()
    async def on_guild_emojis_update(self, guild, before, after) -> None:
        if guild.id not in ROO_EMOJI_IDS:
            return

        channel = discord.utils.get(guild.channels, name="emoji-list")
        if not channel:
            return

        rm_emoji = [f"{emoji} -- `{emoji.id}`" for emoji in before if emoji not in after]
        mk_emoji = [f"{emoji} -- `{emoji}`" for emoji in after if emoji not in before]
        if len(rm_emoji) != 0:
            msg = "⚠ Emoji Removed: "
            msg += ", ".join(rm_emoji)
            await channel.send(msg)
        if len(mk_emoji) != 0:
            msg = "✅ Emoji Added: "
            msg += ", ".join(mk_emoji)
            await channel.send(msg)

async def setup(bot: RoboDanny):
    await bot.add_cog(Emoji(bot))
