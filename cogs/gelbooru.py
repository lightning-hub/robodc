import urllib.parse
from typing import Optional

import discord
from discord.ext import commands, menus

class GelbooruPost:
    def __init__(self, payload):
        self.id = payload['id']
        self.tags = payload['tags'].split(" ")
        self.file_url = payload['file_url']
        self.rating = payload['rating']

    def __repr__(self):
        return f"<GelbooruPost id={self.id} rating={self.rating} file_url={self.file_url}>"

class GelbooruMenu(menus.ListPageSource):
    def __init__(self, data):
        super().__init__(data, per_page=1)

    async def format_page(self, menu, entry: GelbooruPost) -> discord.Embed:
        embed = discord.Embed(title=entry.id,
                              description=f"**Rating**: {entry.rating.capitalize()}\n**Tags**: {entry.tags}",
                              color=discord.Color.blurple())
        embed.set_image(url=entry.file_url)
        embed.set_footer(text=f"Page {menu.current_page + 1}/{len(self.entries)}")
        return embed

class Gelbooru(commands.Cog):
    def __init__(self, bot) -> None:
        self.bot = bot

    @commands.is_nsfw()
    @commands.command()
    async def gelbooru(self, ctx, limit: Optional[int], *, tags) -> None:
        """Searches for images on gelbooru"""
        tags = urllib.parse.quote(tags, safe="")
        limit = limit or 20
        
        async with self.bot.session.get(f"https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&limit={limit}&tags={tags}") as resp:
            _data = await resp.json()
            data = [GelbooruPost(x) for x in _data]

        for x in data:
            bad_tags = any(tag in ['loli', 'cub', 'shota', 'child'] for tag in x.tags)
            if bad_tags:
                data.remove(x)

        if not data:
            raise commands.BadArgument("You provided bad tags, the tags you searched are blacklisted, or no "
                                       "results were found. \N{SHRUG}")

        pages = menus.MenuPages(GelbooruMenu(data), delete_message_after=True, check_embeds=True)
        await pages.start(ctx)

async def setup(bot):
    await bot.add_cog(Gelbooru(bot))
